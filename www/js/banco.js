/*Creamos una clase 'Banco' añadiendo a su constructor un nombre y una dirección, además de dos arrays, uno al que se incorporen los clientes que abran una cuenta en el banco, y otro en el que 
estén aquellos clientes que fueran bloqueados por el banco.*/

class Banco {
    constructor(name, address) {
        this.name = name;
        this.address = address;
        this.clients = [];
        this.blockedClients = [];
    }

/*Definimos dos métodos. El primero permitirá bloquear a un usuario, filtrándolo del array de clientes y pasándolo al de clientes bloqueados.*/
    bloquearCuenta(titular) {

        const block = this.clients.filter(client => {
            return { nombre: titular.nombre, id: titular.id }
        });

        this.blockedClients.push(block);

        console.log(`Las cuentas del titular '${titular.nombre}' han sido bloqueadas.`)
    }
    
/*El segundo método desbloqueará a un cliente realizando el proceso inverso al anterior: filtrándolo del array de clientes bloqueados y pasándolo al de quienes no lo están.*/
    desbloquearCuenta(titular) {

        const unblock = this.blockedClients.filter(client => {
            return { nombre: titular.nombre, id: titular.id }
        });

        this.clients.push(unblock);

        console.log(`Las cuentas del titular '${titular.nombre}' han sido desbloqueadas.`)
    }

}

/*La siguiente clase que crearemos será la de 'CuentaBancaria', que tendrá en su constructor un id por definir, un balance que comenzará en cero, y un acceso que inicialmente equivaldrá 
a 'true'.*/
class CuentaBancaria {
    constructor(id) {
        this.balance = 0;
        this.id = id;
        this.access = true;
    }

/*El único método de esta clase bloqueará el acceso del titular a la cuenta en caso de que el id de la cuenta y el del titular no sean el mismo, cambiando el valor del acceso a 'false'.*/
    bloquearAcceso(cuenta, titular) {

        if (cuenta.id !== titular.id) {
            cuenta.access = false;

            console.log(`El acceso a la cuenta con id '${cuenta.id}' del titular '${titular.nombre}' ha sido bloqueado.`)
        }
        else {
            cuenta.access = true;
        }
    }
}

/*La última clase será la del 'Titular', con nombre, género, monedero y una id en el constructor.*/
class Titular {
    constructor(nombre, genero, monedero) {
        this.nombre = nombre;
        this.genero = genero;
        this.monedero = monedero;
        this.id = Math.floor(Math.random() * 999999999);
    }

/*Esta clase tendrá varios métodos: uno de ellos le permitirá abrirse una cuenta en un banco, creando una id para dicha cuenta y añadiendo al titular al array de clientes del banco.*/
    abrirCuentaBancaria(banco) {
        const idBanco = this.id;
        banco.clients.push({ nombre: this.nombre, id: this.id });
        return new CuentaBancaria(idBanco);
    }

/*Ahora pasaremos a los métodos que influirán sobre el balance de la cuenta. Para que el titular pueda ingresar dinero, el acceso a su cuenta debe equivaler a 'true' y que la cantidad que
quiera ingresar no sobrepase a la que tiene almacenada en su monedero. Si alguna de estas condiciones no se cumpliera, se le notificará del motivo del error.*/
    ingresarDinero(cantidad, cuenta) {

        if (cuenta.access === true) {

            if (this.monedero - cantidad < 0) {
                console.log('No hay suficiente dinero en el monedero.')
            }
            else {
                this.monedero -= cantidad;
                cuenta.balance += cantidad;
                console.log(`Has ingresado ${cantidad} euros.`)
            }
        }
        else {
            console.log('No puedes acceder a esta cuenta.');
        }
    }

/*Si quiere retirar dinero, las condiciones son similares a las del método anterior, simplemente cambiando que la cantidad que quiera retirar no supere a la que tenga en el balance de la
cuenta. También se le notificará al titular si ocurriera algún error.*/
    retirarDinero(cantidad, cuenta) {

        if (cuenta.access === true) {

            if (cuenta.balance - cantidad < 0) {
                console.log('No hay suficiente dinero en la cuenta bancaria.')
            }
            else {
                cuenta.balance -= cantidad;
                this.monedero += cantidad;
                console.log(`Has retirado ${cantidad} euros.`);
            }
        }
        else {
            console.log('No puedes acceder a esta cuenta.');
        }
    }

/*Por último, si el usuario quiere saber de cuanto efectivo dispone en su cuenta, simplemente deberá tener el acceso a la misma de manera que equivalga a 'true'. Si no es así, se le hará
saber.*/
    mostrarSaldo(cuenta) {

        if (cuenta.access === true) {
            console.log(`Tienes un total de ${cuenta.balance} euros.`);
        }
        else {
            console.log('No puedes acceder a esta cuenta.');
        }
    }

}

/*Finalmente creamos las instancias para cada clase y procedemos a comprobar que todos los métodos definidos en dichas clases funcionan para cada una de estas 
instancias.*/
const manolo = new Titular('Manolo', 'hombre', 1200);
const bancoSantander = new Banco('Banco Santander', 'Ronda de Outeiro 52, A Coruña');
const cuentaManolo = manolo.abrirCuentaBancaria(bancoSantander);

bancoSantander.bloquearCuenta(manolo);
bancoSantander.desbloquearCuenta(manolo);
cuentaManolo.bloquearAcceso(cuentaManolo, manolo);
manolo.ingresarDinero(1100, cuentaManolo);
manolo.retirarDinero(100, cuentaManolo);
manolo.mostrarSaldo(cuentaManolo);