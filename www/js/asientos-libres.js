const bussyDesks = [12, 2, 6, 7, 11];

//El primer elemento del array es el número de asientos, así que lo sacamos y lo almacenamos en una nueva variable
const numberOfDesks = bussyDesks.shift();

//Declaramos una variable contador que nos permitirá llevar la cuenta del número de asientos libres que están uno al lado del otro.
let count = 0;

//Creamos una función con un bucle for que compruebe si el asiento i está dentro del array o no.
function searchDesks(array) {
    for (let i = 1; i < array; i++) {
        if (bussyDesks.indexOf(i) === -1) {
            
            //Después comprobamos si el asiento i es par o impar. En caso de ser impar, comprobamos si los dos asientos siguientes están libres o no. Si lo están, actualizamos el contador.
            if (i % 2 !== 0) {
                for (let j = i + 1; j <= i + 2; j++) {
                    if (bussyDesks.indexOf(j) === -1) {
                        count += 1;
                    }
                }
            }
            
            //En caso de ser par solo comprobamos si el asiento siguiente está libre. Si lo está, actualizamos el contador.
            else {
                for (let j = i + 2; j <= i + 2; j++) {
                    if (bussyDesks.indexOf(j) === -1) {
                        count += 1;
                    }
                }
            }
        }
    }
    return count;
}

//Por último llamamos a la función y sacamos por la consola la variable contador, que tendrá el número de asientos libres que están el uno al lado del otro.
searchDesks(numberOfDesks);

console.log(count);