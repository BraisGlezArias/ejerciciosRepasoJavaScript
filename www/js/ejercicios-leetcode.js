//Contains Duplicate:

/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
    /*Declaramos una variable que evaluará false o true según si hay duplicados en el array o no.*/
    let duplicate = false;
    
    /*Creamos un for dentro de otro for para comparar cada valor del array con el resto, si hay dos valores iguales, la variable anterior será true, sino será false. Por último devolvemos
    el valor de dicha variable.*/
    for (let i=0; i < nums.length; i++) {
        for (let j=i+1; j < nums.length; j++) {
            if (nums[i] === nums[j]) {
                duplicate = true;
            }
        } 
    }
    return duplicate;    
};

//Max Consecutive Ones:

/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxConsecutiveOnes = function(nums) {
    
    /* Declaramos dos variables, una que lleva la cuenta de los unos consecutivos y otra que almacene el máximo de unos consecutivos. Ambas empiezan en cero.*/
    let maxCount = 0;
    let count = 0;
    
    /* Usamos un 'for' para recorrer el array. Si encuentra un 1, la cuenta de la variable 'count' aumenta y si, eventualmente, supera al de la variable 'MaxCount', esta pasa a tener dicho valor.
    Si se encuentra un cero, el valor de 'count' vuelve a cero, y así hasta que termine el bucle. Finalmente devolvemos el valor del máximo de unos consecutivos.*/
    for (let i=0; i < nums.length; i++) {
        if (nums[i] === 1) {
            count = count + 1;
            if (count > maxCount) {
                maxCount = count;
            }
        }
        else {
            count = 0;
       }
    }
    return maxCount;    
};

//Move Zeroes:

/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function(nums) {
    
    /*Usamos un 'for' para recorrer el array en busca de ceros, y cuando encontramos alguno, lo 'spliceamos' y lo guardamos en una nueva variable. Seguidamente 'pusheamos' el valor de esta variable
    en el array. Por último devolvemos el array inicial con los cambios pertinentes.*/
    for (let i=nums.length; i >= 0; i--) {
        if (nums[i] === 0) {
            let j = nums.splice(i, 1);
            nums.push(j);
        }
    }

return nums;
    
};