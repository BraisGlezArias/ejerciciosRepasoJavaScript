class Robot {
    /*Incorporamos al constructor de la clase 'Robot* un espacio por el que se moverán sus instancias y las coordenadas del punto de origen de las mismas.*/
    constructor(space) {
        this.space = space;
        this.rows = 0;
        this.columns = 0;
    }
    
    /*Pasamos a dar forma a los distintos métodos que usará la instancia para moverse. En el caso de moverse hacía la izquierda, se desplazará negativamente una unidad en la coordenada horizontal.
    Pero en caso de salirse del espacio, no se moverá de su situación actual.*/
    moveLeft() {
        if (this.rows === 0) {
            return false;
        }
        else {
            this.rows = this.rows - 1;
        }
    }
    
    /*En el caso de moverse hacía la derecha, se desplazará positivamente una unidad en la coordenada horizontal.
    Pero en caso de salirse del espacio, no se moverá de su situación actual.*/
    moveRight() {
        if (this.rows === this.space[this.rows].length - 1) {
            return false;
        }
        else {
            this.rows = this.rows + 1;
        }
    }
    /*En el caso de moverse hacía arriba, se desplazará negativamente una unidad en la coordenada vertical.
    Pero en caso de salirse del espacio, no se moverá de su situación actual.*/
    moveUp() {
        if (this.columns === 0) {
            return false;
        }
        else {
            this.columns = this.columns - 1;
        }
    }
    
    /*En el caso de moverse hacía abajo, se desplazará positivamente una unidad en la coordenada vertical.
    Pero en caso de salirse del espacio, no se moverá de su situación actual.*/
    moveDown() {
        if (this.columns === this.space[this.columns].length - 1) {
            return false;
        }
        else {
            this.columns = this.columns + 1;
        }

    }
    
    /*El último método permitirá saber cuál es la posición de la instancia en cualquier momento dado.*/
    currentPosition() {
        return this.space[this.rows][this.columns];
    }
}

/*Por último, definimos un espacio y una instancia para la clase Robot, y pasamos a llamar a los distintos métodos para mover dicha instancia, comprobando posteriormente que se
mueve de acuerdo a como hemos programado los métodos mencionados, sacando su posición actual tras cada movimiento por la consola.*/ 
const mySpace = [[1, 9, 5],
[8, 7, 6],
[6, 5, 9]];
const myLittlePrettyRobot = new Robot(mySpace);

myLittlePrettyRobot.moveRight();
myLittlePrettyRobot.moveDown();
console.log(myLittlePrettyRobot.currentPosition());

myLittlePrettyRobot.moveLeft();
myLittlePrettyRobot.moveUp();
console.log(myLittlePrettyRobot.currentPosition());

myLittlePrettyRobot.moveLeft()
myLittlePrettyRobot.moveUp();
console.log(myLittlePrettyRobot.currentPosition());

myLittlePrettyRobot.moveRight();
myLittlePrettyRobot.moveRight();
myLittlePrettyRobot.moveRight();
console.log(myLittlePrettyRobot.currentPosition());

myLittlePrettyRobot.moveDown();
myLittlePrettyRobot.moveDown();
myLittlePrettyRobot.moveDown();
console.log(myLittlePrettyRobot.currentPosition());