/**
 * ###################################
 * ###### E J E R C I C I O   1 ######
 * ###################################
 * 
 * Sara y Laura juegan al baloncesto en diferentes equipos. En los
 * últimos 3 partidos, el equipo de Sara anotó 89, 120 y 103 puntos,
 * mientras que el equipo de Laura anotó 116, 94, y 123 puntos.
 *
 * `1.` Calcula la media de puntos para cada equipo.
 *
 * `2.` Muestra un mensaje que indique cuál de los dos equipos
 *      tiene mejor puntuación media. Incluye en este mismo mensaje
 *      la media de los dos equipos.
 *
 * `3.` María también juega en un equipo de baloncesto. Su equipo
 *      anotó 97, 134 y 105 puntos respectivamente en los últimos
 *      3 partidos. Repite los pasos 1 y 2 incorporando al equipo
 *      de María.
 *
 */

const sara = [89, 120, 103];
const laura = [116, 94, 123];
const maria = [97, 134, 105];

/* Primero vamos a aplicar la función reduce a cada uno de los arrays para que nos dé la suma de los números.
Para reducir el tamaño del código, primero creamos una variable que recoja los argumentos que necesita reduce y los sume mediante
una 'arrowfunction'. De paso sacamos por la consola la suma de cada equipo.*/
const reducer = (accumulator, currentValue) => accumulator + currentValue;

const mediaSara = sara.reduce(reducer) / (sara.length);
const mediaLaura = laura.reduce(reducer) / (laura.length);
const mediaMaria = maria.reduce(reducer) / (maria.length);

console.log(`El equipo de Sara tiene una media de ${mediaSara} puntos`);
console.log(`El equipo de Laura tiene una media de ${mediaLaura} puntos`);
console.log(`El equipo de María tiene una media de ${mediaMaria} puntos`);

/*Por último aplicamos la función Math.max para hallar la puntuación máxima de entre los tres equipos y usamos un switch para sacar
por la consola al equipo con la mayor puntuación.*/
const best = Math.max(mediaSara, mediaLaura, mediaMaria);

switch (best) {
    case mediaSara:
        console.log('El equipo con mejor puntuación media es el de Sara');
        break;
    case mediaLaura:
        console.log('El equipo con mejor puntuación media es el de Laura');
        break;
    case mediaMaria:
        console.log('El equipo con mejor puntuación media es el de María');
        break;
    default:
        console.log('Ningún equipo tiene la mejor puntuación');
}

/**
 * ###################################
 * ###### E J E R C I C I O   2 ######
 * ###################################
 *
 * Jorge y su familia han ido a comer a tres restaurantes distintos.
 * La factura fue de 124€, 58€ y 268€ respectivamente.
 *
 * Para calcular la propina que va a dejar al camarero, Jorge ha
 * decidido crear un sistema de calculo (una función). Quiere
 * dejar un 20% de propina si la factura es menor que 50€, un 15%
 * si la factura está entre 50€ y 200€, y un 10% si la factura es
 * mayor que 200€.
 *
 * Al final, Jorge tendrá dos arrays:
 *
 * - `Array 1` Contiene las propinas que ha dejado en cada uno de
 *    los tres restaurantes.
 *
 * - `Array 2` Contiene el total de lo que ha pagado en cada uno de
 *    los restaurantes (sumando la propina).
 *
 * `NOTA` Para calcular el 20% de un valor, simplemente multiplica
 *  por `0.2`. Este resultado se obtiene de dividir `20/100`. Si
 *  quisieramos averiguar el 25% de un valor lo multiplicaríamos
 *  por 0.25.
 *
 * `25 / 100 = 0.25`.
 *
 */

/*Junto al array de precios estándar, creamos otros dos arrays vacíos que contendrán las propinas y los precios totales respectivamente.*/
const arr = [124, 58, 268];

let propina = [];

let total = [];

/*Creamos una función en la que mediante un for 'pusheemos' la cantidad de propina que se pagó en cada caso en su array correspondiente,
y mediante otro for sumamos estos valores con los precios estándar para obtener los precios totales y 'pushearlos' en su array.*/
function darPropina(arr) {

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 50) {
            let prop = arr[i] * 0.2;
            propina.push(prop);
        }
        else if (arr[i] >= 50 && arr[i] <= 200) {
            let prop = arr[i] * 0.15;
            propina.push(prop);
        }
        else if (arr[i] > 200) {
            let prop = arr[i] * 0.1;
            propina.push(prop);
        }
    }

    for (let j = 0; j < arr.length; j++) {
        let tot = arr[j] + propina[j];
        total.push(tot);
    }
}

/*Por último llamamos a la función y sacamos por la consola los arrays de propinas y precios totales.*/

darPropina(arr);
console.log(propina);
console.log(total);

/**
* ###################################
* ###### E J E R C I C I O   3 ######
* ###################################
*
* Dado el siguiente array de números:
*
* `nums = [100, 3, 4, 2, 10, 4, 1, 10]`
*
* `1.` Recorre todo el array y muestra por consola cada uno de sus
*      elementos con la ayuda de un `for`, con la ayuda de un `map`
*      y con la ayuda de un `for...of`.
*
* `2.` Ordena el array de menor a mayor sin emplear `sort()`.
*
* `3.` Ordena el array de mayor a menor empleando `sort()`.
*
*/

const nums = [100, 3, 4, 2, 10, 4, 1, 10];

/*Usamos un for sencillo, un map y un for of para mostrar por consolas los elementos del array.*/
for (let i = 0; i < nums.length; i++) {
    console.log(nums[i])
}

const nums2 = nums.map(function (num) {
    console.log(num)
});

for (const num of nums) {
    console.log(num)
}

/*Para ordenar los elementos del array sin recurrir a la función sort, creamos otra función en la que declararemos una variable como false
y usaremos un bucle for dentro de un while para que vaya comparando cada número del array con el anterior y recolocándonos de menor a mayor.
Cuando todos los elementos estén ordenados, la variable cambiará a true y el bucle terminará.*/
function sorting(arr) {
    let end = false;
    while (!end) {
        end = true;
        for (let i = 1; i < arr.length; i++) {
            if (arr[i - 1] > arr[i]) {
                end = false;
                let j = arr[i - 1];
                arr[i - 1] = arr[i];
                arr[i] = j;
            }
        }
    }
    return arr;
}

sorting(nums);
console.log(nums)

/*Para ordenar el array de mayor a menor usando la función sort, simplemente tenemos que cambiar el orden de los argumentos de la misma.*/
nums.sort(function (a, b) {
    return b - a;
});

console.log(nums)

/**
* ###################################
* ###### E J E R C I C I O   4 ######
* ###################################
*
* Crea una `arrow function` que reciba dos números por medio del
* `prompt`, reste ambos números, y nos devuelva el resultado.
* En caso de que el resultado sea negativo debe cambiarse a
* positivo. Este resultado se mostrará por medio de un `alert`.
*
*/

/*Creamos dos variables que pidan dos números al usuario con 'prompt'*/
let num1 = prompt('Dame un número');
let num2 = prompt('Dame otro número');

/*Declaramos una variable que comprenda una 'arrowfunction' en la que si el resultado de la resta de los dos números es negativa, se
vuelva positivia, y si no, se quede tal y cómo está.*/
let resta = ((a, b) => {
    if ((a - b) < 0) {
        return -(a - b);
    }
    else {
        return a - b;
    }
});

/*Por último llamamos a la función dentro de otra variable y usamos una 'alert' para mostrarle el resultado al usuario*/

const resultado = resta(num1, num2);

alert(resultado);